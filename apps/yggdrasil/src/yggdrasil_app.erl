-module(yggdrasil_app).
-behaviour(application).
-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
  case yggdrasil_sup:start_link() of
    { ok, Pid } ->
      ok = riak_core:register(yggdrasil, [{vnode_module, yggdrasil_vnode}]),
      ok = riak_core_ring_events:add_guarded_handler(yggdrasil_ring_event_handler, []),
      ok = riak_core_node_watcher_events:add_guarded_handler(yggdrasil_node_event_handler, []),
      ok = riak_core_node_watcher:service_up(yggdrasil, self()),
      { ok, Pid };
    { error, Reason } ->
      { error, Reason }
  end.

stop(_State) ->
  ok.
