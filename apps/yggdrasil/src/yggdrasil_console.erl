-module(yggdrasil_console).
-export([join/1, leave/1, remove/1, commit/1, plan/1, ringready/1, transfers/1]).

join([NodeStr]) ->
  try riak_core:join(NodeStr) of % original
    ok ->
      io:format("Sent join request to ~s.~n", [NodeStr]),
      ok;
    {error, not_reachable} ->
      io:format("Node ~s is not reachable!~n", [NodeStr]),
      error;
    {error, different_ring_sizes} ->
      io:format("Failed: ~s has a different ring_creation_size.~n", [NodeStr]),
      error
  catch
    Exception:Reason ->
      lager:error("Join failed ~p:~p~n", [Exception, Reason]),
      io:format("Join failed, see log for details.~n"),
      error
  end;
join(_) ->
  io:format("Join requires a node to join to.~n"),
  error.

leave([]) ->
  riak_core_console:stage_leave(node()).

remove([Node]) ->
  riak_core_console:stage_remove(Node).

commit([]) ->
  riak_core_console:commit_staged([]).

plan([]) ->
  riak_core_console:print_staged([]).

%leave([]) ->
%  remove_node(node()).
%
%remove([Node]) ->
%  remove_node(list_to_atom(Node)).
%
%remove_node(Node) when is_atom(Node) ->
%  %try catch(riak_core:remove_from_cluster(Node)) of % original
%  %try catch(riak_core_gossip:remove_from_cluster(Node)) of
%  try catch(riak_core:leave(Node)) of
%  {'EXIT', {badarg, [{erlang, hd, [[]]}|_]}} ->
%      io:format("Leave failed, this node is the only member.~n"),
%      error;
%    Res ->
%      io:format("Unknown Result for yggdrasil_console:remove_node(): ~p~n", [Res]),
%      ok
%  catch
%    Exception:Reason ->
%      lager:error("Leave failed ~p:~p~n", [Exception, Reason]),
%      io:format("Leave failed, see log for details.~n"),
%      error
%  end.

-spec(ringready([]) -> ok | error).
ringready([]) ->
  try riak_core_status:ringready() of
    {ok, Nodes} ->
      io:format("TRUE All nodes agree on the ring ~p.~n", [Nodes]);
    {error, {different_owners, N1, N2}} ->
      io:format("FALSE Node ~p and ~p list different partition owners.~n", [N1, N2]),
      error;
    {error, {nodes_down, Down}} ->
      io:format("FALSE ~p down, All nodes need to be up to check.~n", [Down]),
      error
  catch
    Exception:Reason ->
      lager:error("Ringready failed ~p:~p~n", [Exception, Reason]),
      io:format("Ringready failed, see log for details.~n"),
      error
  end.

-spec(transfers([]) -> ok).
transfers([]) ->
  {DownNodes, Pending} = yggdrasil_status:transfers(),
  case DownNodes of
    [] -> ok;
    _  -> io:format("Nodes ~p are currently down.~n", [DownNodes])
  end,
  F = fun({waiting_to_handoff, Node, Count}, Acc) ->
    io:format("~p waiting to handoff ~p partitions.~n", [Node, Count]),
    Acc + 1;
  ({stopped, Node, Count}, Acc) ->
    io:format("~p does not have ~p primary partitions running.~n", [Node, Count]),
    Acc + 1
  end,
  case lists:foldl(F, 0, Pending) of
    0 -> io:format("No transfers active.~n"), ok;
    _ -> error
  end.
