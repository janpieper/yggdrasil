-module(yggdrasil_node_event_handler).
-behaviour(gen_event).

-export([
  %% Start & Stop
  init/1, terminate/2,
  %% Handlers
  handle_event/2, handle_call/2, handle_info/2,
  %% Code changes
  code_change/3
]).

-record(state, {}).

init([]) ->
  {ok, #state{}}.

handle_event({service_update, _Services}, State) ->
  {ok, State}.

handle_call(_Event, State) ->
  {ok, ok, State}.

handle_info(_Info, State) ->
  {ok, State}.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

terminate(_Reason, _State) ->
  ok.
