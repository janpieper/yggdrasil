-module(yggdrasil).

-include("yggdrasil.hrl").
-include_lib("riak_core/include/riak_core_vnode.hrl").

-export([ping/0, hello/0]).

hello() ->
  command(hello).

ping() ->
  command(ping).

command(Cmd) ->
  CmdBin = list_to_binary(atom_to_list(Cmd)),
  DocIdx = riak_core_util:chash_key({CmdBin, term_to_binary(now())}),
  PrefList = riak_core_apl:get_primary_apl(DocIdx, 1, yggdrasil),
  [{IndexNode, _Type}] = PrefList,
  riak_core_vnode_master:sync_spawn_command(IndexNode, Cmd, yggdrasil_vnode_master).
