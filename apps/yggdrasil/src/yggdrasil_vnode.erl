-module(yggdrasil_vnode).
-behaviour(riak_core_vnode).

-include_lib("riak_core/include/riak_core_vnode.hrl").
-include("yggdrasil.hrl").

-export([
  % VNode
  start_vnode/1, terminate/2, handle_exit/3, init/1,
  % Callbacks
  is_empty/1, delete/1,
  % Handoff
  handoff_starting/2, handoff_cancelled/1, handoff_finished/2,
  handle_handoff_command/3, handle_handoff_data/2, encode_handoff_item/2,
  % Coverage
  handle_coverage/4,
  % Commands
  handle_command/3
]).

% https://github.com/seancribbs/riak_id/blob/master/apps/riak_id/src/riak_id_vnode.erl

%% Callbacks

is_empty(State) ->
  {true, State}.

delete(State) ->
  {ok, State}.

%% VNode

start_vnode(I) ->
  riak_core_vnode_master:get_vnode_pid(I, ?MODULE).

init([Index]) ->
  TS = erlang:now(),
  <<MachineID:10/bits, _Rest/bits>> = <<Index:160/integer>>,
  {ok, #state{idx = Index, machine_id = MachineID, last_timestamp = TS}}.

terminate(_Reason, _State) ->
  ok.

handle_exit(_Pid, Reason, State) ->
  {stop, Reason, State}.

%% Commands

handle_command(hello, Sender, State) ->
  ?PRINT({current_sender, Sender}),
  ?PRINT({current_state, State}),
  {reply, <<"Hello world">>, State};
handle_command(ping, _Sender, State) ->
  {reply, pong, State};
handle_command(Message, _Sender, State) ->
  ?PRINT({unhandled_command, Message}),
  {noreply, State}.

%% Handoff

handoff_starting(_TargetNode, State) ->
  {true, State}.

handoff_cancelled(State) ->
  {ok, State}.

handoff_finished(_TargetNode, State) ->
  {ok, State}.

handle_handoff_command(_Message, _Sender, State) ->
  {forward, State}.

handle_handoff_data(_Data, State) ->
  {reply, ok, State}.

encode_handoff_item(_ObjectName, _ObjectValue) ->
  <<>>.

%% Coverage

handle_coverage(_Req, _KeySpaces, _Sender, State) ->
  {stop, not_implemented, State}.
