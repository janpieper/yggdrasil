-module(yggdrasil_sup).
-behaviour(supervisor).

-export([
  %% API
  start_link/0,
  %% Supervisor callbacks
  init/1
]).

%% ============================================================================
%% API functions
%% ============================================================================

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ============================================================================
%% Supervisor callbacks
%% ============================================================================

init(_Args) ->
  %webmachine_router:add_route({["id"], yggdrasil_wm, []}),
  VMaster = {yggdrasil_vnode_master,
    {riak_core_vnode_master, start_link, [yggdrasil_vnode]},
    permanent, 5000, worker, [riak_core_vnode_master]
  },
  {ok, {{one_for_one, 5, 10}, [VMaster]}}.
