-module(yggdrasil_status).
-export([transfers/0]).

-spec(transfers() -> {[atom()], [{waiting_to_handoff, atom(), integer()} | {stopped, atom(), integer()}]}).
transfers() ->
  {Down, Rings} = get_rings(),
  F = fun({N, R}, Acc) ->
    {_Pri, Sec, Stopped} = partitions(N, R),
    Acc1 = case Sec of
      [] -> [];
      _  -> [{waiting_to_handoff, N, length(Sec)}]
    end,
    case Stopped of
      [] -> Acc1 ++ Acc;
      _  -> Acc1 ++ [{stopped, N, length(Stopped)} | Acc]
    end
  end,
  {Down, lists:foldl(F, [], Rings)}.

get_rings() ->
  {RawRings, Down} = riak_core_util:rpc_every_member(
    riak_core_ring_manager, get_my_ring, [], 30000
  ),
  Rings = orddict:from_list([{riak_core_ring:owner_node(R), R} || {ok, R} <- RawRings]),
  {lists:sort(Down), Rings}.

partitions(Node, Ring) ->
  Owners = riak_core_ring:all_owners(Ring),
  Owned = ordsets:from_list(owned_partitions(Owners, Node)),
  Active = ordsets:from_list(active_partitions(Node)),
  Stopped = ordsets:subtract(Owned, Active),
  Secondary = ordsets:subtract(Active, Owned),
  Primary = ordsets:subtract(Active, Secondary),
  {Primary, Secondary, Stopped}.

owned_partitions(Owners, Node) ->
  [P || {P, Owner} <- Owners, Owner =:= Node].

active_partitions(Node) ->
  F = fun({_, P}, Ps) -> ordsets:add_element(P, Ps) end,
  lists:foldl(F, [], running_vnodes(Node)).

running_vnodes(Node) ->
  Pids = vnode_pids(Node),
  [rpc:call(Node, riak_core_vnode, get_mod_index, [Pid], 30000) || Pid <- Pids].

vnode_pids(Node) ->
  [Pid || {_, Pid, _, _} <- supervisor:which_children({riak_core_vnode_sup, Node})].
